# TO LOAD Tuple_Turbo.py, NEED TO ADD THIS LOCATION TO PYTHON PATH
import os,sys
sys.path.append(os.environ['TRKCALIBROOT']+'/python')

from Tuple_Turbo import set_Tuples

#######################
### INPUT           ###
#######################
datatype = "2017"        # FOR ALL CATEGORIES, SEE cond_db_dict IN Tuple_Turbo_test.py
sample = "Data"          # DATA, MAG UP OR MAG DOWN

# CALL FUNCTION THAT SETS DAVINCI OPTIONS WHICH DEPEND ON DATATYPE AND SAMPLE
set_Tuples(datatype,sample,MDST=False,Test=False)

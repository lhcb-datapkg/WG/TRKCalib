import sys
sys.path.append("/afs/cern.ch/user/m/mmulder/TAeff")

from Tuple_Turbo import set_Tuples

#######################
### INPUT           ###
#######################
datatype = "2016"        # FOR ALL CATEGORIES, SEE cond_db_dict IN Tuple_Turbo.py
sample = "Data"          # DATA, MAG UP OR MAG DOWN

# CALL FUNCTION THAT SETS DAVINCI OPTIONS WHICH DEPEND ON DATATYPE AND SAMPLE
set_Tuples(datatype,sample,MDST=True,Test=True)


